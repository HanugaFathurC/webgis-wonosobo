//Distance measurement

var drawLine;
var drawLayer = new ol.layer.Vector({
  source: new ol.source.Vector(),
});
map.addLayer(drawLayer);

function measureDistance() {
  drawLine = new ol.interaction.Draw({
    source: drawLayer.getSource(),
    type: "LineString",
  });
  map.addInteraction(drawLine);

  drawLine.on("drawend", function (evt) {
    console.log(evt);
    var distanceMeasureParam = new SuperMap.MeasureParameters(evt.feature.getGeometry());
    new ol.supermap.MeasureService(url, {
      measureMode: "",
    }).measureDistance(distanceMeasureParam, function (serviceResult) {
      console.log(serviceResult);
      alert(serviceResult.result.distance.toFixed(2) + " m", true);
    });
  });
}

//Area Measurement
var drawPolygon;
function measureArea() {
  drawPolygon = new ol.interaction.Draw({
    source: drawLayer.getSource(),
    type: "Polygon",
  });
  map.addInteraction(drawPolygon);

  drawPolygon.on("drawend", function (evt) {
    console.log(evt);
    var distanceMeasureParam = new SuperMap.MeasureParameters(evt.feature.getGeometry());
    new ol.supermap.MeasureService(url, {
      measureMode: "",
    }).measureArea(distanceMeasureParam, function (serviceResult) {
      console.log(serviceResult);
      alert(serviceResult.result.area.toFixed(2) + " m²", true);
    });
  });
}

//Clear draw
function clearDraw() {
  map.removeInteraction(drawLine);
  map.removeInteraction(drawPolygon);
  drawLayer.getSource().clear();
}

//fullExtent
function fullExtent() {
  map.setView(
    new ol.View({
      center: [109.905703, -7.357818],
      zoom: 13,
      projection: "EPSG:4326",
      multiWorld: true,
    })
  );
}

var container = document.getElementById("popup"),
  content = document.getElementById("popup-content");
var overlay = new ol.Overlay({
  element: container,
  autoPan: true,
  autoPanAnimation: {
    duration: 250,
  },
  offset: [0, -20],
});
